<html>
<head>

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<script type="text/javascript" src="scriptChat.js"></script>
<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start();
require_once("Modeles/PDOcanin.php");
$PDOCanin = PDOCanin::getPDOCanin();
require_once("Modeles/fonctions.php");

?><div id="container"><?php
include("Vues/Entete.php");

if(!isset($_GET["uc"])){
	$uc='Accueil';
}
else{
	$uc = $_GET['uc'];
}

switch($uc){
	case 'Accueil':{
		if(isset($_SESSION["id"]) && !isset($_SESSION["admin"])){
			$dataClient = $PDOCanin->verifDocs($_SESSION["id"]);
		}
		if(isset($_SESSION["admin"])){
			$dataAdmin = $PDOCanin->verifDemandes();
		}
		include("Vues/Accueil.php");
		break;
	}
	case 'Contact':{
		include("Vues/Contact.php");
		break;
	}
	case 'Demande':{
		if($PDOCanin->EnvoiDemande($_POST["Nom"], $_POST["Prenom"], $_POST["Mail"], $_POST["textarea"])){
			echo "Votre demande a été envoyé à l'administrateur. Vous receverez vos identifiants de connexion par mail si celle-ci est acceptée.";
		}
		break;
	}
	case 'MesChiens':{
		include("Controleurs/MesChiens.php");
		break;
	}
	case 'MonCompte':{
		include("Controleurs/MonCompte.php");
		break;
	}
	case 'LesComptes':{
		include("Controleurs/LesComptes.php");
		break;
	}
	case 'Connexion':{
		if(isset($_GET["erreur"])){
			$message_erreur = "Login ou Mot de passe incorrect.";
		}
		include("Vues/Connexion.php");
		break;
	}
	case 'Valid_Connexion':{
		$connexion = $PDOCanin->Connexion($_POST["login"], $_POST["mdp"]);
		if($connexion){
			header("Location: index.php");
			exit;
		}
		else{
			header("Location: index.php?uc=Connexion&erreur");
			exit;
		}
		break;
	}
	case 'Deconnexion':{
		session_destroy();
		header("Location: index.php");
		exit;
	}
	case 'Reglement':{
		include("Vues/Reglement.html");
		break;
	}
}
?>
</div>
<div id="chatA">
<?php
include("Vues/Chat.php");
?>
</div>
<div id="chat">

</div>

<script type="text/javascript">

	function alert_mdp(){
		alert("Mettre quelque chose si vous voulez modifier le mdp.");
	}
</script>
</body>
</html>