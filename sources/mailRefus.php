<?php
// Import PHPMailer classes into the global namespace
	// These must be at the top of your script, not inside a function
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	//Load composer's autoloader
	require 'vendor/autoload.php';

	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	try {
		//Server settings
		$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = '80.12.242.10';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = false;                               // Enable SMTP authentication
		//$mail->Username = 'user@example.com';                 // SMTP username
		//$mail->Password = 'secret';                           // SMTP password
		//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 25;                                    // TCP port to connect to

		//Recipients
		$mail->setFrom('Meute.Citadine@NoReply.com', 'Meute Citadine');
		$mail->addAddress($client[0]["Email"], $client[0]["Nom"]);     // Add a recipient
	   // $mail->addAddress('ellen@example.com');               // Name is optional
		//$mail->addReplyTo('info@example.com', 'Information');
		//$mail->addCC('cc@example.com');
		//$mail->addBCC('bcc@example.com');

		//Attachments
		//$mail->addAttachment('./Images/Logo.png>');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'Demande refusée';
		$mail->Body    =  '<Center><Center><img src="https://image.noelshack.com/fichiers/2017/41/3/1507735226-logo.png" alt="Logo"><h2>Meute Citadine </h2></center> </br>  Bonjour, Votre inscrption sur notre site n\' as pas etait accepter ! </br></h6>Pour toute questions merci de contacter l\'administrateur sur notre site : <a href="http://172.17.0.8/Canin/index.php?uc=Contact">Meute Citadine</a></br></br> <font size="3" face="georgia" color="red">PS: Ceci est un email automatique, merci de ne pas y repondre !</font>';
		//$mail->AltBody = 'Bonjour, Votre inscription a reussi, voici vos identifiant :\n Votre Login : '.$client[0]["Login"].' \n Votre Mot de Passe : '.$_SESSION["mdp_mail"].' <br /> <br /> PS: Ceci est un email automatique, merci de ne pas y repondre !';

		$mail->send();
		echo 'Message has been sent';
	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
?>
?>