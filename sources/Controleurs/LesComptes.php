<?php
if(!isset($_SESSION["admin"])){
	header("Location: index.php");
	exit;
}
if(!isset($_GET["action"])){
	$action='Menu';
}
else{
	$action = $_GET['action'];
}

switch($action){
	case 'Menu':{		
		include("Vues/Menu_admin.php");
		break;
	}
	case 'AffichageClients':{		
		$listeClients = $PDOCanin->Clients();
		include("Vues/AffichageClients.php");
		break;
	}
	case 'AffichageDemandes':{	
		$listeDemandes = $PDOCanin->Demandes();	
		include("Vues/AffichageDemandes.php");
		break;
	}
	case 'Creation':{
		$PDOCanin->CreationCompte($_GET["id"]);
		$client = $PDOCanin->Client($_GET["id"]);
		include("mailDemande.php");
		unset($_SESSION["mdp_mail"]);
		break;
	}	
	case 'SupprimerDemande':{
		$client = $PDOCanin->Client($_GET["id"]);
		$PDOCanin->SupprimerDemande($_GET["id"]);		
		include("mailRefus.php");
		break;
	}
	case 'SupprimerClient':{
		$client = $PDOCanin->Client($_GET["id"]);
		$PDOCanin->SupprimerClient($_GET["id"]);
		break;
	}
	case 'Modifier':{
		$_SESSION["id_modif"] = $_GET["id"];
		$data = $PDOCanin->Client($_SESSION["id_modif"]);
		include("Vues/ModifierClient.php");
		break;
	}
	case 'Valid_Modif':{
		$PDOCanin->ModifClient($_SESSION["id_modif"], $_POST["nom"], $_POST["prenom"], $_POST["dateN"], $_POST["rue"], $_POST["cp"], $_POST["ville"], $_POST["email"], $_POST["tel"]);
		unset($_SESSION["id_modif"]);
		header("Location: index.php?uc=LesComptes&action=AffichageClients");
		break;
	}
}
?>