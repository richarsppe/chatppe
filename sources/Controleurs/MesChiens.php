<?php
if(!isset($_SESSION["id"])){
	header("Location: index.php");
	exit;
}

if(!isset($_GET["action"])){
	$action='Menu';
}
else{
	$action = $_GET['action'];
}

switch($action){
	case 'Menu':{
		$data = $PDOCanin->Chiens($_SESSION['id']);
		include("Vues/AffichageChiens.php");
		break;
	}
	case 'Modifier':{
		$_SESSION["puce_modif"] = $_GET["puce"];
		$chien = $PDOCanin->Chien($_SESSION["puce_modif"]);
		include("Vues/ModifierChien.php");
		break;
	}
	case 'Valid_modif':{
		$data = uploadFiles($_SESSION["puce_modif"]);
		$PDOCanin->ModifChien($_SESSION["puce_modif"], $_POST["nom"], $_POST["dateN"], $_POST["lof"]);
		$PDOCanin->ChienDocs($_SESSION["puce_modif"], $data[0], $data[1], $data[2], $data[3], $data[4]);
		unset($_SESSION["puce_modif"]);
		header("Location: index.php?uc=MesChiens");
		break;
	}
	case 'Ajout':{
		$races = $PDOCanin->Races();
		include("Vues/AjoutChien.php");
		break;
	}
	case 'Valid_ajout':{
		mkdir("./Documents/".$_POST["puce"], 0777);
		$data = uploadFiles($_POST["puce"]);		
		$PDOCanin->AjoutChien($_POST["puce"], $_POST["nom"], $_POST["race"], $_POST["dateN"], $_POST["lof"]);
		$PDOCanin->ChienDocs($_POST["puce"], $data[0], $data[1], $data[2], $data[3], $data[4]);
		header("Location: index.php?uc=MesChiens");
		break;
	}
}
?>