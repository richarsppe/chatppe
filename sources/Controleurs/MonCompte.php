<?php
if(!isset($_SESSION["id"])){
	header("Location: index.php");
	exit;
}

if(!isset($_GET["action"])){
	$action = "Donnees";
}
else{
	$action = $_GET["action"];
}

switch($action){
	case 'Donnees':{
		$donnees = $PDOCanin->Client($_SESSION["id"]);
		include("Vues/DonneesCompte.php");
		break;
	}
	case 'Modification':{
		$donnees = $PDOCanin->Client($_GET["id"]);
		include("Vues/ModifierCompte.php");
		break;
	}
	case 'Valid_Modification':{
		$PDOCanin->ModifClient($_SESSION["id"], $_POST["nom"], $_POST["prenom"], $_POST["dateN"], $_POST["rue"], $_POST["cp"], $_POST["ville"], $_POST["email"], $_POST["tel"], $_POST["mdp"]);
		break;
	}
}
?>