-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 15 Octobre 2017 à 14:39
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `canin`
--

-- --------------------------------------------------------

--
-- Structure de la table `adhesion`
--

DROP TABLE IF EXISTS `adhesion`;
CREATE TABLE IF NOT EXISTS `adhesion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DateAdhesion` date DEFAULT NULL,
  `DatePayement` date DEFAULT NULL,
  `Puce` int(11) NOT NULL DEFAULT '0',
  `Adherent` int(11) DEFAULT NULL,
  `Carte_chien` varchar(200) DEFAULT NULL,
  `Certificat_naissance` varchar(200) DEFAULT NULL,
  `Certificat_antirabique` varchar(200) DEFAULT NULL,
  `Declaration_mairie` varchar(200) DEFAULT NULL,
  `Attestation_aptitude` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_adhesion_dogs` (`Puce`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `idUser` int(11) NOT NULL,
  `commentaire` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `dogs`
--

DROP TABLE IF EXISTS `dogs`;
CREATE TABLE IF NOT EXISTS `dogs` (
  `Puce` int(11) NOT NULL,
  `Nom` char(50) DEFAULT NULL,
  `Race` int(11) DEFAULT NULL,
  `Date_Naissance` date DEFAULT NULL,
  `Num_Lof` int(11) DEFAULT NULL,
  PRIMARY KEY (`Puce`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `race`
--

DROP TABLE IF EXISTS `race`;
CREATE TABLE IF NOT EXISTS `race` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle` char(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=307 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Nom` char(50) DEFAULT NULL,
  `Prenom` char(50) DEFAULT NULL,
  `Date_Naissance` date DEFAULT NULL,
  `Rue` char(100) DEFAULT NULL,
  `CP` char(5) DEFAULT NULL,
  `Ville` char(50) DEFAULT NULL,
  `Email` char(50) DEFAULT NULL,
  `Tel` char(10) DEFAULT NULL,
  `Login` char(50) DEFAULT '',
  `MDP` char(50) DEFAULT '',
  `Admin` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `adhesion`
--
ALTER TABLE `adhesion`
  ADD CONSTRAINT `FK_adhesion_dogs` FOREIGN KEY (`Puce`) REFERENCES `dogs` (`Puce`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `FK_commentaire_users` FOREIGN KEY (`idUser`) REFERENCES `users` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
