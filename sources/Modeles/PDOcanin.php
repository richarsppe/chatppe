<?php
class PdoCanin
{
      	private static $serveur='mysql:host=172.17.0.8';
      	private static $bdd='dbname=Canin';
      	private static $user='kevin' ;
      	private static $mdp='SIO2.KR';
		private static $monPdo;
		private static $monPdoCanin = null;

	private function __construct()
	{
    		PdoCanin::$monPdo = new PDO(PdoCanin::$serveur.';'.PdoCanin::$bdd, PdoCanin::$user, PdoCanin::$mdp);
			PdoCanin::$monPdo->query("SET CHARACTER SET utf8");
	}

	public static function getPdoCanin()
	{
		if(PdoCanin::$monPdoCanin == null)
		{
			PdoCanin::$monPdoCanin= new PdoCanin();
		}
		return PdoCanin::$monPdoCanin;
	}

	//Constructeur
	public function Connexion($Login, $MDP)
	{
		$req = "SELECT ID, Login, MDP, Admin FROM users WHERE `Login` = '".$Login."' AND `MDP` = MD5('".$MDP."')";
		$res = PdoCanin::$monPdo->query($req);
		$lesLignes = $res->rowCount();
		if ($lesLignes==1)
		{
			$res = PdoCanin::$monPdo->query($req);
			$lesLignes = $res->fetchAll();
			$_SESSION["login"] = $lesLignes[0]["Login"];
			$_SESSION["id"] = $lesLignes[0]["ID"];
			if($lesLignes[0]["Admin"] == 1){

				$_SESSION["admin"] = $lesLignes[0]["Admin"];
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	//Vérification si le login exite ou non dans la BDD
	public function VerifLogin($Login)
	{
		$req2 = "Select Login From users Where `Login` = '".$Login."'";
		$res2 = PdoCanin::$monPdo->query($req2);
		$lesLignes = $res2->rowCount();
		return $lesLignes;
	}

	//Envoi une demande d'inscription qui sera visible par l'administrateur
	public function EnvoiDemande($Nom, $Prenom, $Mail, $Commentaire)
	{
		//Pour eviter les doubles demandes
		$req="SELECT * FROM users WHERE `Nom` = '".$Nom."' AND `Prenom` = '".$Prenom."' AND `Email` = '".$Mail."'";
		$res = PdoCanin::$monPdo->query($req);
		$lesLignes = $res->rowCount();
		if ($lesLignes!=1)
		{
			$req = "INSERT INTO `users` (`Nom`, `Prenom`, `Email`) VALUES ('".$Nom."', '".$Prenom."', '".$Mail."');";
			PdoCanin::$monPdo->query($req);
		}
		$req2="select max(ID) as id from users where Nom='".$Nom."' and Prenom='".$Prenom."'";
		$res2 = PdoCanin::$monPdo->query($req2);
		$id = $res2->fetchAll();
		$req3 = "insert into commentaire values (".$id[0][0].", '".addslashes($Commentaire)."')";
		PdoCanin::$monPdo->query($req3);
		return true;
	}

	//Créer un compte pour un utlisateur en générant login et mdp qui lui seront envoyés par mail
	public function CreationCompte($ID)
	{
		$req = "SELECT * FROM users WHERE ID=".$ID;
		$res = PdoCanin::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		$Prenom=$lesLignes[0]["Prenom"];
		$Nom=$lesLignes[0]["Nom"];
		$login = CreationL($Prenom, $Nom, $this);
		$mdp = CreationM();
		$_SESSION["mdp_mail"] = $mdp;
		$req = "UPDATE users SET Login ='".$login."', MDP='".MD5($mdp)."' WHERE ID =".$ID;
		PdoCanin::$monPdo->query($req);
	}

	//Renvoie toutes les races de chiens de la BDD
	public function Races(){
		$req = "select * from race";
		$res = PdoCanin::$monPdo->query($req);
		return $res;
	}

	//Renvoie tous les comptes utilisateurs non admin
	public function Clients(){
		$req = "SELECT * FROM users WHERE Login != '' AND MDP != '' and Admin!=1";
		$res = PdoCanin::$monPdo->query($req);
		if($res){
			$lesLignes = $res->fetchAll();
			return $lesLignes;
		}
		else{
			return false;
		}
	}

	//Rencoi toutes les demandes de la BDD
	public function Demandes()
	{
		$req = "SELECT users.*, commentaire.commentaire FROM users join commentaire on users.ID = commentaire.idUser WHERE Login = '' AND MDP = ''";
		$res = PdoCanin::$monPdo->query($req);
		if($res){
			$lesLignes = $res->fetchAll();
			return $lesLignes;
		}
		else{
			return false;
		}
	}

	//Supprime une demande de la BDD
	public function SupprimerDemande($id)
	{
		$req = "DELETE FROM users WHERE ID=".$id;
		PdoCanin::$monPdo->query($req);
		$req2 = "delete from commentaire where idUser=".$id;
		PdoCanin::$monPdo->query($req2);
	}

	//Supprime un client de la BDD ainsi que les adhesions liées
	//Problème lors de la suppression des fichiers sur le serveur avec les méthodes unlink et rmdir
	public function SupprimerClient($id)
	{
		$req = "delete from adhesion where Adherent=".$id;
		PdoCanin::$monPdo->query($req);
		$req2 = "DELETE FROM users WHERE ID=".$id;
		PdoCanin::$monPdo->query($req2);
	}

	//Renvoie les informations d'un client
	public function Client($id){
		$req = "select * from users where ID =".$id;
		$res = PdoCanin::$monPdo->query($req);
		$data = $res->fetchAll();
		return $data;
	}

	//Modfie les informations d'un client
	public function ModifClient($id, $nom, $prenom, $dateN, $rue, $cp, $ville, $email, $tel, $mdp){
		if($mdp == ''){
			$req = "update users set Nom='".$nom."', Prenom='".$prenom."', Date_Naissance='".$dateN."', Rue='".$rue."', CP='".$cp."', Ville='".$ville."', Email='".$email."', Tel='".$tel."' where ID=".$id;
			PdoCanin::$monPdo->query($req);
		}
		else{
			$req = "update users set Nom='".$nom."', Prenom='".$prenom."', Date_Naissance='".$dateN."', Rue='".$rue."', CP='".$cp."', Ville='".$ville."', Email='".$email."', Tel='".$tel."', MDP='".MD5($mdp)."' where ID=".$id;
			PdoCanin::$monPdo->query($req);
		}
	}

	//Renvoie tous les chiens de la BDD
	public function Chiens($id){
		$req = "select dogs.Nom, adhesion.Puce, adhesion.DateAdhesion, adhesion.DatePayement from adhesion join dogs on adhesion.Puce = dogs.Puce where adhesion.Adherent =".$id;
		$res = PdoCanin::$monPdo->query($req);
		$data = $res->fetchAll();
		return $data;
	}

	//Renvoie les inforamtions d'un chien
	public function Chien($puce){
		$req = "select dogs.Nom, dogs.Date_Naissance, dogs.Num_Lof, adhesion.* from adhesion join dogs on adhesion.Puce = dogs.Puce where adhesion.Puce =".$puce;
		$res = PdoCanin::$monPdo->query($req);
		$data = $res->fetchAll();
		return $data;
	}

	//Ajoute un chien dans la BDD
	public function AjoutChien($puce, $nom, $race, $dateN, $lof){
		$req = "insert into dogs values (".$puce.", '".$nom."', '".$race."', '".$dateN."', ".$lof.")";
		PdoCanin::$monPdo->query($req);
		$req2 = "insert into adhesion (DateAdhesion, Puce, Adherent) values('".date('Y-m-d')."', ".$puce.", ".$_SESSION["id"].")";
		PdoCanin::$monPdo->query($req2);
	}

	//Modifie les informations d'un chien dans la BDD
	public function ModifChien($puce, $nom, $dateN, $lof){
		$req = "update dogs set Nom='".$nom."', Date_Naissance='".$dateN."', Num_Lof='".$lof."' where Puce=".$puce;
		PdoCanin::$monPdo->query($req);
	}

	//Envoie les liens des documents dans la BDD
	public function ChienDocs($puce, $carteChien, $naissance, $antirabique, $mairie, $aptitude){
		$i = 0;
		$chaine = "";
		if($carteChien != ""){
			$chaine = "Carte_chien='".$carteChien."'";
			$i = 1;
		}
		if($naissance != ""){
			if($i != 0){
				$chaine = $chaine.", Certificat_naissance='".$naissance."'";
			}
			else{
				$chaine = "Certificat_naissance='".$naissance."'";
				$i = 2;
			}
		}
		if($antirabique != ""){
			if($i != 0){
				$chaine = $chaine.", Certificat_antirabique='".$antirabique."'";
			}
			else{
				$chaine = "Certificat_antirabique='".$antirabique."'";
				$i = 3;
			}
		}
		if($mairie != ""){
			if($i != 0){
				$chaine = $chaine.", Declaration_mairie='".$mairie."'";
			}
			else{
				$chaine = "Declaration_mairie='".$mairie."'";
				$i = 4;
			}
		}
		if($aptitude != ""){
			if($i != 0){
				$chaine = $chaine.", Attestation_aptitude='".$aptitude."'";
			}
			else{
				$chaine = "Attestation_aptitude='".$aptitude."'";
				$i = 5;
			}
		}
		$req = "update adhesion set ".$chaine." where Puce=".$puce;
		PdoCanin::$monPdo->query($req);
	}

	//Vérifie si tous les documents sont enregistrés dans la BDD
	public function verifDocs($id){
		$req = "select dogs.Nom, adhesion.Puce, Carte_chien, Certificat_naissance, Certificat_antirabique, Declaration_mairie, Attestation_aptitude from adhesion join dogs on adhesion.Puce = dogs.Puce where Adherent=".$id;
		$res = PdoCanin::$monPdo->query($req);
		if($res){
			$data = $res->fetchAll();
			return $data;
		}
		else{
			return false;
		}
	}

	//Vérifie si il y a des nouvelles demandes dans la BDD
	public function verifDemandes(){
		$req = "select * from commentaire";
		$res = PdoCanin::$monPdo->query($req);
		if($res){
			$data = $res->fetchAll();
			return $data;
		}
		else{
			return false;
		}
	}

  public function envoyerMessage($Message)
  {
    $req = "INSERT INTO `messages` (`message`) VALUES ('".$Message."');";
  }
}
?>
