<?php
//Creation d'un login pour un compte
function CreationL($Prenom, $Nom, $Pdo)
	{		
		$Login= substr($Prenom, 0, 1);
		$Login=$Login.$Nom;
		$CPT=0;
		$lesLignes=$Pdo->VerifLogin($Login);
		while($lesLignes>0)
		{
			$CPT++;
			$Login=$Login."$CPT";
			$lesLignes=$Pdo->VerifLogin($Login);
		}				
		return $Login;			
	}	

//Creation d'un mot de passe aléatoire
function CreationM()
{
	$caract="azertyuiopqsdfghjklmwxcvbn0123456789@+-#!=*";
	$cpt=0;
	$mdp = "";	
	while($cpt!=8)
	{
		$nb=rand(0,44);
		$mdp=$mdp.substr($caract, $nb, 1);
		$cpt++;
	}
	return $mdp;
}

//Récupération des fichiers uploadés (taille, extensionn nom et nom temporaire) lors de l'ajout ou la modification d'un chien
function uploadFiles($puce){
	$taille = filesize($_FILES['carte_chien']['tmp_name']);
	$extension = strrchr($_FILES['carte_chien']['name'], '.'); 
	$carteChien = upload($puce, basename($_FILES['carte_chien']['name']),$_FILES['carte_chien']['tmp_name'], $taille, $extension);
	$taille = filesize($_FILES['naissance']['tmp_name']);
	$extension = strrchr($_FILES['naissance']['name'], '.'); 
	$naissance = upload($puce, basename($_FILES['naissance']['name']),$_FILES['naissance']['tmp_name'], $taille, $extension);
	$taille = filesize($_FILES['antirabique']['tmp_name']);
	$extension = strrchr($_FILES['antirabique']['name'], '.'); 
	$antirabique = upload($puce, basename($_FILES['antirabique']['name']),$_FILES['antirabique']['tmp_name'], $taille, $extension);
	$taille = filesize($_FILES['mairie']['tmp_name']);
	$extension = strrchr($_FILES['mairie']['name'], '.'); 
	$mairie = upload($puce, basename($_FILES['mairie']['name']),$_FILES['mairie']['tmp_name'], $taille, $extension);
	$taille = filesize($_FILES['aptitude']['tmp_name']);
	$extension = strrchr($_FILES['aptitude']['name'], '.'); 
	$aptitude = upload($puce, basename($_FILES['aptitude']['name']),$_FILES['aptitude']['tmp_name'], $taille, $extension);
	$data = array($carteChien, $naissance, $antirabique, $mairie, $aptitude);
	return $data;
}

//Upload d'un image dans le dossier du chien
function upload($puce, $name, $tmp_name, $taille, $extension){
	if($name != ""){
		$dossier = './Documents/'.$puce.'/';
		$taille_maxi = 1000000;
		$extensions = array('.png', '.gif', '.jpg', '.jpeg');
		//Début des vérifications de sécurité...
		if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
		{
			 $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg.';
		}
		if($taille>$taille_maxi)
		{
			 $erreur = 'Le fichier est trop gros.';
		}
		if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
		{
			//On formate le nom du fichier ici...
			$name = strtr($name, 
			'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
			'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
			$name = preg_replace('/([^.a-z0-9]+)/i', '-', $name);
			if(move_uploaded_file($tmp_name, $dossier.$name)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné.
			{
				return $dossier.$name;
			}
			else //Sinon (la fonction renvoie FALSE).
			{
				return 'Echec de l\'upload.';
			}
		}
		else
		{
			 return $erreur;
		}
	}	
}
?>